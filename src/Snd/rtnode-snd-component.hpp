/***************************************************************************
 *                                                                         *
 *   rtnode-snd-component.hpp                                              *
 *                                                                         *
 *   Sender -- RTnode base board OROCOS component                          *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef OROCOS_RTNODE_SND_COMPONENT_HPP
#define OROCOS_RTNODE_SND_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include "rtnode-snd-activity.hpp"

#include "msgRTnodeBaseOut.h"

class RtnodeSnd : public RTT::TaskContext{
public:
  RtnodeSnd(std::string const& name);
protected:
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
private:
  int                 	     	       _priority;
  unsigned            	     	       _cpu;
  RTT::InputPort<sMsgRTnodeBaseOut_t>  _inPort;
  RtnodeSndAct                        *_snd;
  sMsgRTnodeBaseOut_t                  _msg;

  bool               _setRemIp(std::string const& ip);
  const std::string& _getRemIp();

  bool               _setRemPort(int port);
  int                _getRemPort();

  bool               _setSndPort(int port);
  int                _getSndPort();

  bool               _setVerbose(bool verbose);
  bool               _getVerbose();


  bool               _msgId(int id, int gid);
  bool               _msgSnd(int ch0, 
			     int ch1, 
			     int o0, 
			     int o1,
			     int mask);


  bool               _setHold(bool hold);
  bool               _getHold();
  
};

#endif
