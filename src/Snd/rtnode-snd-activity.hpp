/***************************************************************************
 *                                                                         *
 *   rtnode-snd-activity.cpp                                               *
 *                                                                         *
 *   Sender -- RTnode base board OROCOS activity                           *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef OROCOS_RTNODE_SND_ACTIVITY_HPP
#define OROCOS_RTNODE_SND_ACTIVITY_HPP

#include <rtt/RTT.hpp>

#include <rtt/os/Mutex.hpp>

#include <string>

#include <sys/socket.h>
#include <netinet/in.h>

#include "msgSeqOut.hpp"

#include "msgRTnodeBaseOut.h"

class RtnodeSndAct : public RTT::Activity{
public:
  RtnodeSndAct(int priority, 
	       unsigned cpu, 
	       std::string const& name);

  bool               store(sMsgRTnodeBaseOut_t const& msg);

  bool               setRemIp(std::string const& ip);
  const std::string& getRemIp();

  bool               setRemPort(int port);
  int                getRemPort();

  bool               setSndPort(int port);
  int                getSndPort();

  bool&              verbose();
  const bool&        verbose() const;

  bool&              hold();
  const bool&        hold() const;

protected:
  bool initialize();
  void step();
  void loop();
  void finalize();
  bool breakLoop();
private:
  bool        	      _run;
  bool                _verbose;
  bool                _hold;
  std::string 	      _remIp;
  int         	      _remPort;
  int         	      _sndPort;
  MsgSeqOut           _seq;
  int                 _fd;
  int                 _sock;
  struct sockaddr_in  _remAddr;
  RTT::os::Mutex      _mutex;
  uint8_t            *_frame;

  bool _send(unsigned int len);

  bool _printFrame(unsigned int len);
};


#endif
