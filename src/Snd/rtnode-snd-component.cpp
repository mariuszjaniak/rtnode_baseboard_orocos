/***************************************************************************
 *                                                                         *
 *   rtnode-snd-component.cpp                                              *
 *                                                                         *
 *   Sender -- RTnode base board OROCOS component                          *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#include "rtnode-snd-component.hpp"

#include <rtt/Component.hpp>
//#include <rtt/Logger.hpp>

#include <string>
#include <iostream>
#include <stdint.h>

#include "msgRTnodeBaseOut.h"

static const int         defPriority = 10;
static const unsigned    defCpu      = 0;

static const std::string errIp("x.x.x.x");

RtnodeSnd::RtnodeSnd(std::string const& name) : 
  TaskContext(name, PreOperational),
  _priority(defPriority),
  _cpu(defCpu)
{
  // Add operations 
  addOperation("setRemIp", 
	       &RtnodeSnd::_setRemIp, 
	       this, 
	       RTT::OwnThread).
    doc("Set remote ip address").
    arg("ip", "IP address (typically broadcast)");
  addOperation("getRemIp", 
	       &RtnodeSnd::_getRemIp, 
	       this, 
	       RTT::OwnThread).
    doc("Get remote ip address");
  addOperation("setRemPort", 
	       &RtnodeSnd::_setRemPort, 
	       this, 
	       RTT::OwnThread).
    doc("Set remote port address").
    arg("port", "port address");
  addOperation("getRemPort", 
	       &RtnodeSnd::_getRemPort, 
	       this, 
	       RTT::OwnThread).
    doc("Get remote port address");
  addOperation("setSndPort", 
	       &RtnodeSnd::_setSndPort, 
	       this, 
	       RTT::OwnThread).
    doc("Set send port address").
    arg("port", "port address");
  addOperation("getSndPort", 
	       &RtnodeSnd::_getSndPort, 
	       this, 
	       RTT::OwnThread).
    doc("Get send port address");
  addOperation("setVerbose", 
	       &RtnodeSnd::_setVerbose, 
	       this, 
	       RTT::OwnThread).
    doc("Set component verbosity").
    arg("verbose", "verbose state (true or false)");
  addOperation("getVerbose", 
	       &RtnodeSnd::_getVerbose, 
	       this, 
	       RTT::OwnThread).
    doc("Get component verbosity");
  addOperation("setHold", 
	       &RtnodeSnd::_setHold, 
	       this, 
	       RTT::OwnThread).
    doc("Set hold").
    arg("hold", "hold state (true or false)");
  addOperation("getHold", 
	       &RtnodeSnd::_getHold, 
	       this, 
	       RTT::OwnThread).
    doc("Get hold state");
  addOperation("id", 
	       &RtnodeSnd::_msgId, 
	       this, 
	       RTT::OwnThread).
    doc("Set message id").
    arg("id",  "Id address").
    arg("gid", "Gid address");
  addOperation("msg", 
	       &RtnodeSnd::_msgSnd, 
	       this, 
	       RTT::OwnThread).
    doc("Send output message").
    arg("ch00", "Analog output 0").
    arg("ch01", "Analog output 1").
    arg("o0",   "Digital output 0").
    arg("o1",   "Digital output 1").
    arg("mask", "Mask output");


  // Add atributes 
  addAttribute("priority", _priority);
  addAttribute("cpu",      _cpu);

  // Add input ports 
  ports()->addEventPort("inPort", _inPort).doc("Sender input port");
}

bool RtnodeSnd::configureHook()
{
  // Set component activitiy 
  if(setActivity(new RTT::Activity(ORO_SCHED_RT, 
				   _priority, 
				   0, 
				   _cpu, 
				   0, 
				   getName())) == false){
    std::cout << "Unable to set activity!" << std::endl;
    return false;
  }

  _snd = new RtnodeSndAct(_priority, _cpu, getName());
  return true;
}

bool RtnodeSnd::startHook(){

  int ret;

  // Check validity of input port:
  // if ( !_inPort.connected() ){
  //   std::cout << "Input port not connected!" std::endl;
  //   return false;
  // }

  // Start sender
  return _snd->start();
}

void RtnodeSnd::updateHook()
{
  sMsgRTnodeBaseOut_t msg;

  if(_inPort.read(msg) == RTT::NewData){
    _snd->store(msg);
  }
}

void RtnodeSnd::stopHook() 
{
  // Stop sender 
  _snd->stop();
}

void RtnodeSnd::cleanupHook() 
{
  delete _snd;
}

bool RtnodeSnd::_setRemIp(std::string const& ip)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  return _snd->setRemIp(ip);
}

const std::string& RtnodeSnd::_getRemIp()
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return errIp;
  }
  return _snd->getRemIp();
}

bool RtnodeSnd::_setRemPort(int port)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  return _snd->setRemPort(port);
}

int RtnodeSnd::_getRemPort()
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return -1;
  }
  return _snd->getRemPort();
}

bool RtnodeSnd::_setSndPort(int port)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  return _snd->setSndPort(port);
}

int RtnodeSnd::_getSndPort()
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return -1;
  }
  return _snd->getSndPort();
}

bool RtnodeSnd::_setVerbose(bool verbose)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  _snd->verbose() = verbose;
  return true;
}

bool RtnodeSnd::_getVerbose()
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  return _snd->verbose();
}

bool RtnodeSnd::_msgId(int id, int gid)
{
  if((id < 0) || (id > 255)){
    std::cout << "Invalid id address!" << std::endl;
    return false;
  }
  if((gid < 0) || (gid > 255)){
    std::cout << "Invalid gid address!" << std::endl;
    return false;
  }
  _msg.id  = id;
  _msg.gid = gid;
  return true;
}

bool RtnodeSnd::_msgSnd(int ch0, int ch1, int o0, int o1, int mask)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return -1;
  }
  _msg.ch00 = (uint16_t) ch0;
  _msg.ch01 = (uint16_t) ch1;
  _msg.o0   = (uint8_t)  o0;
  _msg.o1   = (uint8_t)  o1;
  _msg.mask = (uint8_t)  mask; 
  return _snd->store(_msg);
}

bool RtnodeSnd::_setHold(bool hold)
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  _snd->hold() = hold;
  return true;
}

bool RtnodeSnd::_getHold()
{
  if(!isConfigured()){
    std::cout << "Call configure() first!" << std::endl;
    return false;
  }
  return _snd->hold();
}


/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Rtnode)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(RtnodeSnd)
