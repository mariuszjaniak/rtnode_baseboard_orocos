/***************************************************************************
 *                                                                         *
 *   msgSeqOut.cpp                                                         *
 *                                                                         *
 *   Sequencer for the RTnode base board output message                    *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include "msgSeqOut.hpp"

#include <boost/lexical_cast.hpp>

#define TESTBIT(val, bit) ((val) & (1UL << (bit)))
#define SETBITW(val, bit) (val |=  (1UL << (bit)))
#define CLRBITW(val, bit) (val &= ~(1UL << (bit)))


static const unsigned int msgOutMaskSize = 4;


static const unsigned int defSeqSize     = 50;

MsgSeqOut::MsgSeqOut() : 
  _seq(defSeqSize),
  _len(0)
{
  _msgSize = msgRTnodeBaseOutSerializeSize();
}

bool MsgSeqOut::store(sMsgRTnodeBaseOut_t const& msg)
{
  // Check if empty, add message and return 
  if(_seq.empty()){
    _seq.push_back(msg);
    return true;
  }
  // Check if merging is possible 
  for(unsigned int i = 0; i < _seq.size(); i++){
    if(_testId(msg, _seq[i])){
      return _merge(msg, _seq[i]);  
    }
  }
  // Store message (overwrite the oldest message without notification)
  _seq.push_back(msg);
  return true;
}

unsigned int MsgSeqOut::frame(uint8_t *frame, unsigned int size)
{
  unsigned int len = 0;
  
  for(unsigned int i = size / _msgSize; (i > 0) && !_seq.empty(); i--){
    int ret; 
    ret = msgRTnodeBaseOutSerialize(&_seq.front(), frame + len, size - len);
    if(ret <= 0 ) break;
    len += ret;
    _seq.pop_front();
  }
  return len;
}

bool MsgSeqOut::_testId(sMsgRTnodeBaseOut_t const& msg1, 
			sMsgRTnodeBaseOut_t const& msg2)
{
  if((msg1.id == msg2.id) && (msg1.gid == msg2.gid)) return true;
  return false;
}

bool MsgSeqOut::_merge(sMsgRTnodeBaseOut_t const& src, 
		       sMsgRTnodeBaseOut_t      & dst)
{
  for(unsigned int i = 0; i < msgOutMaskSize; i++){
    if(TESTBIT(src.mask, i)){
      SETBITW(dst.mask, i);
      switch(i){
      case 0:
	dst.ch00 = src.ch00;
	break;
      case 1:
	dst.ch01 = src.ch01;
	break;
      case 2:
	dst.o0 = src.o0;
	break;
      case 3:
	dst.o1 = src.o1;
	break;
      default:
	throw std::logic_error("Not supported field number = "     + 
			       boost::lexical_cast<std::string>(i) +
			       " by the RTnode message out");
      }
    }
  }
  return true;
}


void MsgSeqOut::_printMsg(const sMsgRTnodeBaseOut_t &msg)
{
  std::cout << "  id   = " << (int)msg.id   << std::endl;
  std::cout << "  gid  = " << (int)msg.gid  << std::endl;
  std::cout << "  ch00 = " << (int)msg.ch00 << std::endl;
  std::cout << "  ch01 = " << (int)msg.ch01 << std::endl;
  std::cout << "  o0   = " << (int)msg.o0   << std::endl;
  std::cout << "  o1   = " << (int)msg.o1   << std::endl;
  std::cout << "  mask = " << (int)msg.mask << std::endl;
}

void MsgSeqOut::print()
{
  std::cout << "size = " << _seq.size() << std::endl;
  if(_seq.size() == 0) return;
  for(unsigned int i = 0; i < _seq.size(); i++){
    std::cout << "message " << i << std::endl;
    _printMsg(_seq[i]);

  }
}
