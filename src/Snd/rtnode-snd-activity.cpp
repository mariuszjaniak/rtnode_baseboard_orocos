/***************************************************************************
 *                                                                         *
 *   rtnode-snd-activity.cpp                                               *
 *                                                                         *
 *   Sender -- RTnode base board OROCOS activity                           *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include "rtnode-snd-activity.hpp"

#include <rtnet.h>
#include <rtmac.h>

#include <arpa/inet.h>

#define MTU_SIZE        1500UL
#define ETH_HEADER_SIZE 14UL
#define IP4_HEADER_SIZE 20UL
#define UDP_HEADER_SIZE 8UL

static const unsigned int defFrameSize = 
  MTU_SIZE        - 
  ETH_HEADER_SIZE - 
  IP4_HEADER_SIZE - 
  UDP_HEADER_SIZE;

static const std::string defRemIp("10.255.255.255");
static const uint16_t    defRemPort  = 1524;

static const uint16_t    defSndPort  = 2000;

static const uint32_t    addRtskbs   = 200;

RtnodeSndAct::RtnodeSndAct(int priority, 
			   unsigned cpu, 
			   std::string const& name) : 
  Activity(ORO_SCHED_RT, priority, 0, cpu, 0, name+"_snd"), 
  _run(false),
  _verbose(false),
  _hold(false),
  _remIp(defRemIp),
  _remPort(defRemPort),
  _sndPort(defSndPort)
{
}

bool RtnodeSndAct::initialize()
{
  // Allocate frame
  _frame = new uint8_t [defFrameSize];
  // Set destination address 
  struct in_addr ip;
  if(inet_aton(_remIp.c_str(), &ip) == 0){
    std::cout << "Invalid address: " << _remIp << std::endl;
    return false;
  }
  memset(&_remAddr, 0, sizeof(struct sockaddr_in));
  _remAddr.sin_family = AF_INET;
  _remAddr.sin_port   = htons(_remPort);
  _remAddr.sin_addr   = ip;
  
  // Create rt-socket 
  _sock = rt_dev_socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0) {
    std::cout << " rt_dev_socket() = " << _sock << "!" << std::endl;
    return false;
  }
  // Extend the socket pool 
  int ret;
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_EXTPOOL, &addRtskbs);
  if (ret != (int)addRtskbs) {
    std::cout << "rt_dev_ioctl(RT_IOC_SO_EXTPOOL) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Bind the rt-socket to a port 
  struct sockaddr_in sndAddr;
  memset(&sndAddr, 0, sizeof(struct sockaddr_in));
  sndAddr.sin_family      = AF_INET;
  sndAddr.sin_port        = htons(_sndPort);
  sndAddr.sin_addr.s_addr = INADDR_ANY;
  ret = rt_dev_bind(_sock, 
		    (struct sockaddr *) &sndAddr,
		    sizeof(struct sockaddr_in));
  if (ret < 0) {
    std::cout << "rt_dev_bind() = " << ret << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Open tdma device 
  _fd = rt_dev_open("TDMA0", O_RDWR);
  if (_fd < 0){ 
    std::cout << "rt_dev_open() = " << _fd << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }

  // Succes initialization 
  return true;
}

void RtnodeSndAct::step()
{
  setPeriod(0);
  std::cout << "This is not a periodic activity, switch to period 0!" 
	    << std::endl;
}
 
void RtnodeSndAct::loop()
{
  _run = true;
  while(_run){
    int err;
    // Wait for next RTnet cycle 
    err = rt_dev_ioctl(_fd, RTMAC_RTIOC_WAITONCYCLE, (void*)TDMA_WAIT_ON_SYNC); 
    if(err){  
      std::cout << "Failed to issue RTMAC_RTIOC_WAITONCYCLE, err = " << err 
		<< "!" << std::endl;
    }
    // Check if hold transmition 
    if(_hold) continue;
    // Prepare frame 
    unsigned int len;
    _mutex.lock();
    len = _seq.frame(_frame, defFrameSize);
    _mutex.unlock();
    // Send frame
    if(len > 0){
      if(!_send(len)) 
	std::cout << "Unable to send frame!" << std::endl;
      if(_verbose) _printFrame(len);
    }
  }
}
 
void RtnodeSndAct::finalize()
{
  // Close sender socket 
  while (rt_dev_close(_sock) == -EAGAIN) {
    std::cout << "ERROR: Socket busy - waiting..." << std::endl;
    rt_task_sleep(100);
  }
  // Clode TDMA device
  rt_dev_close(_fd);
  // Free allocated memory
  delete[] _frame;
}

bool RtnodeSndAct::breakLoop()
{
  _run = false;
  //rt_dev_close(_sock);
  return true;
}

bool RtnodeSndAct::store(sMsgRTnodeBaseOut_t const& msg)
{
  if(!isActive()){
    std::cout << "Start first!" << std::endl;
    return false;
  }
  bool ret;
  _mutex.lock();
  ret = _seq.store(msg);
  _mutex.unlock();
  if(_verbose) _seq.print();
  return ret;
}

bool RtnodeSndAct::setRemIp(std::string const& ip)
{
  if(isActive()){
    std::cout << "Stop first!" << std::endl;
    return false;
  }
  struct in_addr addr;
  if(inet_aton(ip.c_str(), &addr) == 0){
    std::cout << "Invalid address" << "!" << std::endl;
    return false;
  }
  _remIp = ip;
  return true;
}

const std::string& RtnodeSndAct::getRemIp()
{
  return _remIp;
}

bool RtnodeSndAct::setRemPort(int port)
{
  if(isActive()){
    std::cout << "Stop first!" << std::endl;
    return false;
  }
  if((port < 0) || (port > 65536)){
    std::cout << "Invalid port number!" << std::endl;
    return false;
  }
  _remPort = port;
  return true;
}

int RtnodeSndAct::getRemPort()
{
  return _remPort;
}

bool RtnodeSndAct::setSndPort(int port)
{
  if(isActive()){
    std::cout << "Stop first!" << std::endl;
    return false;
  }
  if((port < 0) || (port > 65536)){
    std::cout << "Invalid port number!" << std::endl;
    return false;
  }
  _sndPort = port;
  return true;
}

int RtnodeSndAct::getSndPort()
{
  return _sndPort;
}

bool RtnodeSndAct::_send(unsigned int len)
{
  struct msghdr msg;
  struct iovec  iov;

  iov.iov_base    = _frame;
  iov.iov_len     = len;
  memset(&msg, 0, sizeof(msg));
  msg.msg_name    = &_remAddr;
  msg.msg_namelen = sizeof(_remAddr);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;
  
  int ret;
  ret = rt_dev_sendmsg(_sock, &msg, 0);
  if (ret == -EBADF)
    return false;
  else if (ret != (int)len){
    std::cout << " rt_dev_sendmsg() = " << ret << "!" << std::endl;
    return false;
  }
  return true;
}

bool& RtnodeSndAct::verbose()
{
  return _verbose;
}

const bool& RtnodeSndAct::verbose() const
{
  return _verbose;
}

bool& RtnodeSndAct::hold()
{
  return _hold;
}

const bool& RtnodeSndAct::hold() const
{
  return _hold;
}

bool RtnodeSndAct::_printFrame(unsigned int len)
{
  std::cout << "Send frame, len = " << len << std::endl;
  uint32_t 	      proc = 0;
  int                 ret;
  while(proc < len){
    sMsgRTnodeBaseOut_t msg;
    ret = msgRTnodeBaseOutDeSerialize(&msg, _frame + proc, len - proc);
    if(ret <= 0){
      std::cout << "Invalid message at " << proc << std::endl;
      return false;
    }
    proc += ret;
    std::cout << "-----------------------" << std::endl;
    std::cout << "msg.id   = " << (int)msg.id   << std::endl;
    std::cout << "msg.gid  = " << (int)msg.gid  << std::endl;
    std::cout << "msg.ch00 = " << (int)msg.ch00 << std::endl;
    std::cout << "msg.ch01 = " << (int)msg.ch01 << std::endl;
    std::cout << "msg.o0   = " << (int)msg.o0   << std::endl;
    std::cout << "msg.o1   = " << (int)msg.o1   << std::endl;
    std::cout << "msg.mask = " << (int)msg.mask << std::endl;
    std::cout << "-----------------------" << std::endl;
  }
  return true;
}
