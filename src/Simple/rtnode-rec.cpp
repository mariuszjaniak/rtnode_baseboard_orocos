/***************************************************************************
 *                                                                         *
 *   rtnode-rec.cpp                                                        *
 *                                                                         *
 *   RTnode receiver activity                                              *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include "rtnode-rec.hpp"

#include <vector>

#include <rtnet.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "msgRTnodeBaseIn.h"

static const uint32_t addRtskbs = 200;
static const int      defSize   = 1000;

RtnodeRec::RtnodeRec(int priority, 
		     unsigned cpu, 
		     std::string const& name, 
		     int recPort,
		     RTT::OutputPort<sMsgRTnodeBaseIn_t> *outPort) : 
  Activity(ORO_SCHED_RT, priority, 0, cpu, 0, name+"_rec"), 
  _recPort(recPort),
  _run(false),
  _outPort(outPort)
{
  
}


bool RtnodeRec::initialize()
{
  int ret;

  // create rt-socket 
  _sock = rt_dev_socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0) {
    std::cout << " rt_dev_socket() = " << _sock << "!" << std::endl;
    return false;
  }
  // extend the socket pool 
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_EXTPOOL, &addRtskbs);
  if (ret != (int)addRtskbs) {
    std::cout << "rt_dev_ioctl(RT_IOC_SO_EXTPOOL) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // bind the rt-socket to a port 
  struct sockaddr_in recAddr;
  memset(&recAddr, 0, sizeof(struct sockaddr_in));
  recAddr.sin_family      = AF_INET;
  recAddr.sin_port        = htons(_recPort);
  recAddr.sin_addr.s_addr = INADDR_ANY;
  ret = rt_dev_bind(_sock, 
		    (struct sockaddr *) &recAddr,
		    sizeof(struct sockaddr_in));
  if (ret < 0) {
    std::cout << "rt_dev_bind() = " << ret << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Succes initialization 
  return true;
}

void RtnodeRec::step()
{
  setPeriod(0);
  std::cout << "This is not a periodic activity, switch to period 0!" 
	    << std::endl;
}
 
void RtnodeRec::loop()
{
  uint8_t            buf[defSize];
  struct msghdr      msg;
  struct iovec       iov;
  struct sockaddr_in addr;
  
  _run = true;
  while(_run){
    int len;

    iov.iov_base = buf;
    iov.iov_len  = defSize;

    memset(&msg, 0, sizeof(msg));
    msg.msg_name    = &addr;
    msg.msg_namelen = sizeof(addr);
    msg.msg_iov     = &iov;
    msg.msg_iovlen  = 1;

    len = rt_dev_recvmsg(_sock, &msg, 0);
    if(len <= 0) {
      if(_run) std::cout << "rt_dev_recvmsg() = " << len << std::endl;
      _run = false;
      return;
    } 
    // unsigned long ip = ntohl(addr.sin_addr.s_addr);
    // std::cout << "Received packet from " 
    // 	      << ((ip >> 24) & 0xFF) << "." 
    // 	      << ((ip >> 16) & 0xFF) << "." 
    //           << ((ip >>  8) & 0xFF) << "." 
    // 	      << ( ip        & 0xFF) << ", length: " << len << std::endl;

    // Parse frame
    sMsgRTnodeBaseIn_t msg;
    int                ret;
    
    ret = msgRTnodeBaseInDeSerialize(&msg, buf, len);
    if(ret < 0){ 
       std::cout << "Massege is broken!" <<  std::endl;
       continue;
    }

    //   std::cout << "---------------------------" << std::endl;
    //   std::cout << "id   = " << (int) msg.id << std::endl;
    //   std::cout << "gid  = " << (int) msg.gid << std::endl;
    //   std::cout << "ch00 = " << (int) msg.ch00 << std::endl;
    //   std::cout << "ch01 = " << (int) msg.ch01 << std::endl;
    //   std::cout << "ch02 = " << (int) msg.ch02 << std::endl;
    //   std::cout << "ch03 = " << (int) msg.ch03 << std::endl;
    //   std::cout << "i0   = " << (int) msg.i0 << std::endl;
    //   std::cout << "i1   = " << (int) msg.i1 << std::endl;
    //   std::cout << "pos0 = " << msg.pos0 << std::endl;
    //   if(msg.t0 != 0)
    // 	std::cout << "vel0 = " << (double)msg.tic0/msg.t0 << std::endl;
    //   else 
    // 	std::cout << "vel0 = 0" << std::endl;
    //   std::cout << "err0 = " << msg.err0 << std::endl;
    //   std::cout << "pos1 = " << msg.pos1 << std::endl;
    //   if(msg.t1 != 0)
    // 	std::cout << "vel1 = " << (double)msg.tic1/msg.t1 << std::endl;
    //   else 
    // 	std::cout << "vel1 = 0" << std::endl;
    //   std::cout << "err1 = " << msg.err1 << std::endl;
    //   std::cout << "---------------------------" << std::endl;

    _outPort->write(msg);
  }
}
 
void RtnodeRec::finalize()
{
  // while (rt_dev_close(_sock) == -EAGAIN) {
  //   std::cout << "ERROR: Socket busy - waiting..." << std::endl;
  //   rt_task_sleep(100);
  // }
}

bool RtnodeRec::breakLoop()
{
  _run = false;
  rt_dev_close(_sock);
  return true;
}
