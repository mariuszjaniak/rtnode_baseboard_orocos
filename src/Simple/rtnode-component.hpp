/***************************************************************************
 *                                                                         *
 *   rtnode-component.hpp                                                  *
 *                                                                         *
 *   RTnode base board OROCOS component                                    *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#ifndef OROCOS_RTNODE_COMPONENT_HPP
#define OROCOS_RTNODE_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <sys/socket.h>
#include <netinet/in.h>

#include "rtnode-rec.hpp"
#include "msgSeqOut.hpp"

#include "msgRTnodeBaseIn.h"
#include "msgRTnodeBaseOut.h"


class Rtnode : public RTT::TaskContext{
public:
  Rtnode(std::string const& name);
protected:
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
private:
  int                        	       _id;
  int                        	       _gid;
  std::string         	     	       _remIp;
  int                 	     	       _remPort;
  int                 	     	       _sndPort;
  int                 	     	       _recPort;
  int                 	     	       _priority;
  unsigned            	     	       _cpu;
  int                 	     	       _fd;
  int                 	     	       _sock;
  struct sockaddr_in  	     	       _remAddr;
  RTT::InputPort<sMsgRTnodeBaseOut_t>  _inPort;
  RTT::OutputPort<sMsgRTnodeBaseIn_t>  _outPort;
  RtnodeRec                           *_rec;
  bool _setOut(int ch0, int ch1, int o0, int o1, int mask);
};
#endif
