/***************************************************************************
 *                                                                         *
 *   rtnode-rec.hpp                                                        *
 *                                                                         *
 *   RTnode receiver activity                                              *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#ifndef OROCOS_RTNODE_REC_HPP
#define OROCOS_RTNODE_REC_HPP

#include <rtt/RTT.hpp>

#include <string>


#include "msgRTnodeBaseIn.h"

class RtnodeRec : public RTT::Activity{
public:
  RtnodeRec(int priority, 
	    unsigned cpu, 
	    std::string const& name, 
	    int recPort,
	    RTT::OutputPort<sMsgRTnodeBaseIn_t> *outPort);
protected:
  bool initialize();
  void step();
  void loop();
  void finalize();
  bool breakLoop();
private:
  bool _run;
  int  _sock;
  int  _recPort;
  RTT::OutputPort<sMsgRTnodeBaseIn_t> *_outPort;
};
#endif
