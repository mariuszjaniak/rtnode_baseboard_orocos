/***************************************************************************
 *                                                                         *
 *   rtnode-component.cpp                                                  *
 *                                                                         *
 *   RTnode base board OROCOS component                                    *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#include "rtnode-component.hpp"
#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>

#include <string>
#include <iostream>
#include <stdint.h>

#include <rtnet.h>
#include <rtmac.h>

#include <arpa/inet.h>

#include "msgRTnodeBaseCfg.h"
#include "msgRTnodeBaseIn.h"
#include "msgRTnodeBaseOut.h"

#define ID_ALL                  0xFF
#define GID_ALL                 0xFF

#define MSGRTNODEBASEOOUT_MASK_CH0 0x01
#define MSGRTNODEBASEOOUT_MASK_CH1 0x02
#define MSGRTNODEBASEOOUT_MASK_O0  0x04
#define MSGRTNODEBASEOOUT_MASK_O1  0x08

static const uint8_t     defId       = ID_ALL;
static const uint8_t     defGid      = GID_ALL;
static const uint32_t    addRtskbs   = 200;
static const std::string defRemIp("10.0.0.255");
static const uint16_t    defRemPort  = 1524;
static const uint16_t    defSndPort  = 2000;
static const uint16_t    defRecPort  = 1525;
static const int         defPriority = 10;
static const unsigned    defCpu      = 0;

Rtnode::Rtnode(std::string const& name) : 
  TaskContext(name, PreOperational),
  _remIp(defRemIp),
  _remPort(defRemPort),
  _sndPort(defSndPort),
  _recPort(defRecPort),
  _priority(defPriority),
  _cpu(defCpu)
{
  // Add operations 
  addOperation("setOut", 
	       &Rtnode::_setOut, 
	       this, 
	       RTT::OwnThread).
    doc("Set outputs").
    arg("ch00", "Analog output 0").
    arg("ch01", "Analog output 1").
    arg("o0",   "Digital output 0").
    arg("o1",   "Digital output 1").
    arg("mask", "Mask output");
  
  // Add attributes 
  addAttribute("id",       _id);
  addAttribute("gid",      _gid);
  addAttribute("remIp",    _remIp);
  addAttribute("remPort",  _recPort);
  addAttribute("sndPort",  _sndPort);
  addAttribute("recPort",  _recPort);
  addAttribute("priority", _priority);
  addAttribute("cpu",      _cpu);

  // Add input ports 
  ports()->addPort("inPort", _inPort).doc("RTnode component input port");

  // Add output ports
  ports()->addPort("outPort", _outPort ).doc("RTnode component output port");
}

bool Rtnode::configureHook()
{
  // Set component activitiy 
  if(setActivity(new RTT::Activity(ORO_SCHED_RT, 
				   _priority, 
				   0, 
				   _cpu, 
				   0, 
				   getName())) == false){
    std::cout << "Unable to set activity!" << std::endl;
    return false;
  }

  _rec = new RtnodeRec(_priority, _cpu, getName(), _recPort, &_outPort);

  //std::cout << "RTnode configured !" <<std::endl;
  return true;
}

bool Rtnode::startHook(){

  int ret;

  // set destination address 
  struct in_addr ip;
  if(inet_aton(_remIp.c_str(), &ip) == 0){
    std::cout << "Invalid address: " << _remIp << std::endl;
    return false;
  }
  memset(&_remAddr, 0, sizeof(struct sockaddr_in));
  _remAddr.sin_family = AF_INET;
  _remAddr.sin_port   = htons(_remPort);
  _remAddr.sin_addr   = ip;
  
  // create rt-socket 
  _sock = rt_dev_socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0) {
    std::cout << " rt_dev_socket() = " << _sock << "!" << std::endl;
    return false;
  }
  // extend the socket pool 
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_EXTPOOL, &addRtskbs);
  if (ret != (int)addRtskbs) {
    std::cout << "rt_dev_ioctl(RT_IOC_SO_EXTPOOL) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // bind the rt-socket to a port 
  struct sockaddr_in sndAddr;
  memset(&sndAddr, 0, sizeof(struct sockaddr_in));
  sndAddr.sin_family      = AF_INET;
  sndAddr.sin_port        = htons(_sndPort);
  sndAddr.sin_addr.s_addr = INADDR_ANY;
  ret = rt_dev_bind(_sock, 
		    (struct sockaddr *) &sndAddr,
		    sizeof(struct sockaddr_in));
  if (ret < 0) {
    std::cout << "rt_dev_bind() = " << ret << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }

  _fd = rt_dev_open("TDMA0", O_RDWR);
  if (_fd < 0){ 
    std::cout << "rt_dev_open() = " << _fd << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Start receiver thread 
  if(_rec->start() == false){
    rt_dev_close(_sock);
    rt_dev_close(_fd);
    return false;
  }
  //std::cout << "RTnode started !" <<std::endl;
  return true;
}

void Rtnode::updateHook()
{
  int err;

  // Wait for next RTnet cycle 
  err = rt_dev_ioctl(_fd, RTMAC_RTIOC_WAITONCYCLE, (void*)TDMA_WAIT_ON_SYNC); 
  if(err){  
    std::cout << "Failed to issue RTMAC_RTIOC_WAITONCYCLE, err = " << err 
	      << "!" << std::endl;
  }

  // Check if set RTnode outputs 
  sMsgRTnodeBaseOut_t result;
  if (_inPort.read(result) == RTT::NewData){
    std::cout << "InPort: " << std::endl;
    std::cout << "  id:   " << result.id   << std::endl;
    std::cout << "  gid:  " << result.gid  << std::endl;
    std::cout << "  ch00: " << result.ch00 << std::endl;
    std::cout << "  ch01: " << result.ch01 << std::endl;
    std::cout << "  o0:   " << result.o0   << std::endl;
    std::cout << "  o1:   " << result.o1   << std::endl;
    _id  = result.id;
    _gid = result.gid;
    _setOut(result.ch00, result.ch01, result.o0, result.o1, result.mask);
  }

  //std::cout << "RTnode executes updateHook !" <<std::endl;

  // This is special for non periodic activities, it makes the TaskContext call
  // updateHook() again after commands and events are processed.
  getActivity()->trigger(); 
}

void Rtnode::stopHook() 
{
  // Stop receiver 
  _rec->stop();
  // Close sender socket
  while (rt_dev_close(_sock) == -EAGAIN) {
    std::cout << "ERROR: Socket busy - waiting..." << std::endl;
    rt_task_sleep(1000);
  }
  // Clode TDMA device 
  rt_dev_close(_fd);
  //std::cout << "RTnode executes stopping !" <<std::endl;
}

void Rtnode::cleanupHook() {
  delete _rec;
  //std::cout << "RTnode cleaning up !" <<std::endl;
}

bool Rtnode::_setOut(int ch0, int ch1, int o0, int o1, int mask)
{
  if(!isConfigured()){
    std::cout << "Call configure first!" << std::endl;
    return false;
  }

  sMsgRTnodeBaseOut_t msgOut;
  int                 size = msgRTnodeBaseOutSerializeSize();
  int                 ret;
  uint8_t             data[size];

  msgOut.id   = (uint8_t)  _id;
  msgOut.gid  = (uint8_t)  _gid;
  msgOut.ch00 = (uint16_t) ch0;
  msgOut.ch01 = (uint16_t) ch1;
  msgOut.o0   = (uint8_t)  o0;
  msgOut.o1   = (uint8_t)  o1;
  msgOut.mask = (uint8_t)  mask;

  ret = msgRTnodeBaseOutSerialize(&msgOut, data, size);
  if(ret <= 0) return false;

  struct msghdr       msg;
  struct iovec        iov;

  iov.iov_base    = data;
  iov.iov_len     = size;
  memset(&msg, 0, sizeof(msg));
  msg.msg_name    = &_remAddr;
  msg.msg_namelen = sizeof(_remAddr);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;
  
  ret = rt_dev_sendmsg(_sock, &msg, 0);
  if (ret == -EBADF)
    return false;
  else if (ret != (int)size){
    std::cout << " rt_dev_sendmsg() = " << ret << "!" << std::endl;
    return false;
  }
  return true;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Rtnode)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Rtnode)
