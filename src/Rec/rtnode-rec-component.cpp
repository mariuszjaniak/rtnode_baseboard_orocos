/***************************************************************************
 *                                                                         *
 *   rtnode-rec-component.cpp                                              *
 *                                                                         *
 *   Receiver -- RTnode base board OROCOS component                        *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#include "rtnode-rec-component.hpp"
#include <rtt/Component.hpp>
//#include <rtt/Logger.hpp>

#include <string>
#include <iostream>
#include <stdint.h>

#include <rtnet.h>
#include <rtmac.h>

#include <arpa/inet.h>

#include "msgRTnodeBaseIn.h"

#define MTU_SIZE              	1500UL
#define ETH_HEADER_SIZE       	14UL
#define IP4_HEADER_SIZE       	20UL
#define UDP_HEADER_SIZE       	8UL

static const unsigned int defFrameSize = 
  MTU_SIZE        - 
  ETH_HEADER_SIZE - 
  IP4_HEADER_SIZE - 
  UDP_HEADER_SIZE;

static const uint32_t    addRtskbs   = 200;
static const uint16_t    defRecPort  = 1525;
static const int         defPriority = 10;
static const unsigned    defCpu      = 0;
static const unsigned    defTimeout  = 5000; // [ms]

RtnodeRec::RtnodeRec(std::string const& name) : 
  TaskContext(name, PreOperational),
  _run(false),
  _verbose(false),
  _recPort(defRecPort),
  _priority(defPriority),
  _cpu(defCpu),
  _timeout(defTimeout)
{
  // Add attributes 
  addAttribute("verbose",  _verbose);
  addAttribute("recPort",  _recPort);
  addAttribute("priority", _priority);
  addAttribute("cpu",      _cpu);
  addAttribute("timeout",  _timeout);

  // Add output ports
  ports()->addPort("outPort", _outPort).doc("Receiver output port");
}

bool RtnodeRec::configureHook()
{
  // Set component activitiy 
  if(setActivity(new RTT::Activity(ORO_SCHED_RT, 
				   _priority, 
				   0, 
				   _cpu, 
				   0, 
				   getName())) == false){
    std::cout << "Unable to set activity!" << std::endl;
    return false;
  }
  return true;
}

bool RtnodeRec::startHook(){

  int ret;
  
  // Allocate frame
  _frame = new uint8_t [defFrameSize];
  // create rt-socket 
  _sock = rt_dev_socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0) {
    std::cout << " rt_dev_socket() = " << _sock << "!" << std::endl;
    return false;
  }
  // extend the socket pool 
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_EXTPOOL, &addRtskbs);
  if (ret != (int)addRtskbs) {
    std::cout << "rt_dev_ioctl(RT_IOC_SO_EXTPOOL) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Set timeout for message reception 
  nanosecs_rel_t timeout = 1000000LL * _timeout;
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_TIMEOUT, &timeout);
  if(ret < 0){
    std::cout << "rt_dev_ioctl(RTNET_RTIOC_TIMEOUT) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // bind the rt-socket to a port 
  struct sockaddr_in recAddr;
  memset(&recAddr, 0, sizeof(struct sockaddr_in));
  recAddr.sin_family      = AF_INET;
  recAddr.sin_port        = htons(_recPort);
  recAddr.sin_addr.s_addr = INADDR_ANY;
  ret = rt_dev_bind(_sock, 
		    (struct sockaddr *) &recAddr,
		    sizeof(struct sockaddr_in));
  if (ret < 0) {
    std::cout << "rt_dev_bind() = " << ret << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Succes initialization 
  _run = true;
  return true;
}

void RtnodeRec::updateHook()
{
  struct msghdr      msg;
  struct iovec       iov;
  struct sockaddr_in addr;
  int                len;

  iov.iov_base = _frame;
  iov.iov_len  = defFrameSize;

  memset(&msg, 0, sizeof(msg));
  msg.msg_name    = &addr;
  msg.msg_namelen = sizeof(addr);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;

  len = rt_dev_recvmsg(_sock, &msg, 0);
  if(len <= 0) {
    if(_run){
      if(len == -ETIMEDOUT)
	std::cout << "Timeout!" << std::endl;
      else
	std::cout << "rt_dev_recvmsg() = " << len << std::endl;
    }
  } 
  else{
    // unsigned long ip = ntohl(addr.sin_addr.s_addr);
    // std::cout << "Received packet from " 
    // 	      << ((ip >> 24) & 0xFF) << "." 
    // 	      << ((ip >> 16) & 0xFF) << "." 
    //           << ((ip >>  8) & 0xFF) << "." 
    // 	      << ( ip        & 0xFF) << ", length: " << len << std::endl;

    // Parse frame
    sMsgRTnodeBaseIn_t msg;
    int                ret;
    
    ret = msgRTnodeBaseInDeSerialize(&msg, _frame, len);
    if(ret < 0){ 
      std::cout << "Massege is broken!" <<  std::endl;
    }
    else{
      _outPort.write(msg);
      if(_verbose){
        std::cout << "---------------------------" << std::endl;
        std::cout << "id   = " << (int) msg.id << std::endl;
        std::cout << "gid  = " << (int) msg.gid << std::endl;
        std::cout << "ch00 = " << (int) msg.ch00 << std::endl;
        std::cout << "ch01 = " << (int) msg.ch01 << std::endl;
        std::cout << "ch02 = " << (int) msg.ch02 << std::endl;
        std::cout << "ch03 = " << (int) msg.ch03 << std::endl;
        std::cout << "i0   = " << (int) msg.i0 << std::endl;
        std::cout << "i1   = " << (int) msg.i1 << std::endl;
        std::cout << "pos0 = " << msg.pos0 << std::endl;
        if(msg.t0 != 0)
      	std::cout << "vel0 = " << (double)msg.tic0/msg.t0*msg.dir0*1000000000 
		  << std::endl;
        else 
      	std::cout << "vel0 = 0" << std::endl;
        std::cout << "err0 = " << msg.err0 << std::endl;
        std::cout << "pos1 = " << msg.pos1 << std::endl;
        if(msg.t1 != 0)
      	std::cout << "vel1 = " << (double)msg.tic1/msg.t1*msg.dir1*1000000000 
		  << std::endl;
        else 
      	std::cout << "vel1 = 0" << std::endl;
        std::cout << "err1 = " << msg.err1 << std::endl;
        std::cout << "---------------------------" << std::endl;
      }
    }
  }
  // This is special for non periodic activities, it makes the TaskContext call
  // updateHook() again after commands and events are processed.
  if(_run) getActivity()->trigger(); 
}

void RtnodeRec::stopHook() 
{
  _run = false;
  rt_dev_close(_sock);
  // Free allocated memory
  delete[] _frame;
}

void RtnodeRec::cleanupHook() {

}


/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Rtnode)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(RtnodeRec)
