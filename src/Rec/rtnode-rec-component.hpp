/***************************************************************************
 *                                                                         *
 *   rtnode-rec-component.hpp                                              *
 *                                                                         *
 *   Receiver -- RTnode base board OROCOS component                        *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#ifndef OROCOS_RTNODE_REC_COMPONENT_HPP
#define OROCOS_RTNODE_REC_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include "msgRTnodeBaseIn.h"

class RtnodeRec : public RTT::TaskContext{
public:
  RtnodeRec(std::string const& name);
protected:
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
private:
  bool                                 _run;
  bool                                 _verbose;
  int                 	     	       _recPort;
  int                 	     	       _priority;
  unsigned            	     	       _cpu;
  unsigned                             _timeout;   // [ms]
  int                 	     	       _sock;
  uint8_t                             *_frame;
  RTT::OutputPort<sMsgRTnodeBaseIn_t>  _outPort;
};
#endif
