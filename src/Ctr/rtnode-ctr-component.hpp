/***************************************************************************
 *                                                                         *
 *   rtnode-ctr-component.hpp                                              *
 *                                                                         *
 *   Control -- RTnode base board OROCOS component                         *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#ifndef OROCOS_RTNODE_CTR_COMPONENT_HPP
#define OROCOS_RTNODE_CTR_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt/os/TimeService.hpp>


#include "msgRTnodeBaseCtr.h"
#include "msgRTnodeBaseCfg.h"

class RtnodeCtr : public RTT::TaskContext{
public:
  RtnodeCtr(std::string const& name);
protected:
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
private:
  bool                                 _isNew;
  bool                                 _run;
  bool                                 _verbose;
  int                 	     	       _remPort;
  int                 	     	       _sndPort;
  int                 	     	       _priority;
  unsigned            	     	       _cpu;
  unsigned                             _timeout;   // [ms]
  unsigned                             _ctrSize;
  uint8_t                              _id;
  sMsgRTnodeBaseCfg_t                  _cfg;
  int                 	     	       _sock;
  uint8_t                             *_frame;

  int  _send(std::string const& ip, unsigned int size);

  int  _rec();

  int  _prepareFrameCtr(uint8_t type, uint32_t prm);

  int  _prepareFrameCfgNew();

  bool _parseFrameCfgRep(unsigned int size);


  int  _connect(std::string const   &ip, 
		unsigned int         len, 
		sMsgRTnodeBaseCtr_t &msg);

  bool _ping(std::string const& ip);

  bool _reboot(std::string const& ip, unsigned int delay);

  bool _cfgNew(std::string const& ip);

  bool _cfgReq(std::string const& ip);

  const std::string conerr2str(int err);

  const std::string err2str(uint8_t err);

  const std::string type2str(uint8_t type);

  void _printMsgCtr(sMsgRTnodeBaseCtr_t const& msg);

  void _printCfg();

//  bool save(std::string const& name);

};
#endif
