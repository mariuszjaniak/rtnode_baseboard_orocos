/***************************************************************************
 *                                                                         *
 *   rtnode-ctr-component.cpp                                              *
 *                                                                         *
 *   Control -- RTnode base board OROCOS component                         *
 *                                                                         *
 *   Copyright (C) 2015 by Mariusz Janiak                                  *
 *   mariusz.janiak@pwr.edu.pl                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/


#include "rtnode-ctr-component.hpp"
#include <rtt/Component.hpp>
//#include <rtt/Logger.hpp>

#include <string>
#include <iostream>
#include <stdint.h>

#include <rtnet.h>
#include <rtmac.h>

#include <arpa/inet.h>

#include <boost/lexical_cast.hpp>

#include "msgRTnodeBaseCtrDef.h"

#define MTU_SIZE              	1500UL
#define ETH_HEADER_SIZE       	14UL
#define IP4_HEADER_SIZE       	20UL
#define UDP_HEADER_SIZE       	8UL

static const unsigned int defFrameSize = 
  MTU_SIZE        - 
  ETH_HEADER_SIZE - 
  IP4_HEADER_SIZE - 
  UDP_HEADER_SIZE;

static const uint32_t    addRtskbs   = 200;
static const uint16_t    defRemPort  = 1523;
static const uint16_t    defSndPort  = 2001;
static const int         defPriority = 2;
static const unsigned    defCpu      = 0;
static const unsigned    defTimeout  = 5000; // [ms]
static const uint8_t     defId       = 55;

RtnodeCtr::RtnodeCtr(std::string const& name) : 
  TaskContext(name, PreOperational),
  _isNew(false),
  _run(false),
  _verbose(false),
  _remPort(defRemPort),
  _sndPort(defSndPort),
  _priority(defPriority),
  _cpu(defCpu),
  _timeout(defTimeout),
  _id(defId)
{
  // Add operations 
  addOperation("ping", 
  	       &RtnodeCtr::_ping, 
  	       this, 
  	       RTT::OwnThread).
    doc("Send ping to remote RTnode").
    arg("ip", "Ip address");
  addOperation("reboot", 
  	       &RtnodeCtr::_reboot, 
  	       this, 
  	       RTT::OwnThread).
    doc("Reboot RTnode").
    arg("ip", "Ip address").
    arg("delay", "delay time [s]");
  addOperation("cfgNew", 
  	       &RtnodeCtr::_cfgNew, 
  	       this, 
  	       RTT::OwnThread).
    doc("Send new configuration to remote RTnode").
    arg("ip", "Ip address");
  addOperation("cfgReq", 
  	       &RtnodeCtr::_cfgReq, 
  	       this, 
  	       RTT::OwnThread).
    doc("Acquire configuration from remote RTnode").
    arg("ip", "Ip address");
  addOperation("printCfg", 
  	       &RtnodeCtr::_printCfg, 
  	       this, 
  	       RTT::OwnThread).
    doc("Print node configuration");

  // Add attributes 
  addAttribute("verbose",  _verbose);
  addAttribute("remPort",  _remPort);
  addAttribute("sndPort",  _sndPort);
  addAttribute("priority", _priority);
  addAttribute("cpu",      _cpu);
  addAttribute("timeout",  _timeout);

//  addAttribute("cfg",      _cfg);

  addProperty("cfg", _cfg).doc("Node configuration");

  // Set ctr message size
  _ctrSize = msgRTnodeBaseCtrSerializeSize();
}

bool RtnodeCtr::configureHook()
{
  // Set component activitiy 
  if(setActivity(new RTT::Activity(ORO_SCHED_RT, 
				   _priority, 
				   0, 
				   _cpu, 
				   0, 
				   getName())) == false){
    std::cout << "Unable to set activity!" << std::endl;
    return false;
  }
  return true;
}

bool RtnodeCtr::startHook(){

  int ret;
  
  // Allocate frame
  _frame = new uint8_t [defFrameSize];
  // Create rt-socket 
  _sock = rt_dev_socket(AF_INET, SOCK_DGRAM, 0);
  if (_sock < 0) {
    std::cout << " rt_dev_socket() = " << _sock << "!" << std::endl;
    return false;
  }
  // Extend the socket pool 
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_EXTPOOL, &addRtskbs);
  if(ret != (int)addRtskbs){
    std::cout << "rt_dev_ioctl(RT_IOC_SO_EXTPOOL) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Set timeout for message reception 
  nanosecs_rel_t timeout = 1000000LL * _timeout;
  ret = rt_dev_ioctl(_sock, RTNET_RTIOC_TIMEOUT, &timeout);
  if(ret < 0){
    std::cout << "rt_dev_ioctl(RTNET_RTIOC_TIMEOUT) = " << ret << "!" 
	      << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // bind the rt-socket to a port 
  struct sockaddr_in recAddr;
  memset(&recAddr, 0, sizeof(struct sockaddr_in));
  recAddr.sin_family      = AF_INET;
  recAddr.sin_port        = htons(_sndPort);
  recAddr.sin_addr.s_addr = INADDR_ANY;
  ret = rt_dev_bind(_sock, 
		    (struct sockaddr *) &recAddr,
		    sizeof(struct sockaddr_in));
  if (ret < 0) {
    std::cout << "rt_dev_bind() = " << ret << "!" << std::endl;
    rt_dev_close(_sock);
    return false;
  }
  // Succes initialization 
  _run = true;
  return true;
}

void RtnodeCtr::updateHook()
{
  // ???
}

void RtnodeCtr::stopHook() 
{
  _run = false;
  rt_dev_close(_sock);
  // Free allocated memory
  delete[] _frame;
}

void RtnodeCtr::cleanupHook() {

}

int RtnodeCtr::_send(std::string const& ip, unsigned int size)
{
  struct in_addr ipAddr;
  if(inet_aton(ip.c_str(), &ipAddr) == 0){
    std::cout << "Invalid address: " << ip << std::endl;
    return false;
  }

  struct sockaddr_in remAddr;
  memset(&remAddr, 0, sizeof(struct sockaddr_in));
  remAddr.sin_family = AF_INET;
  remAddr.sin_port   = htons(_remPort);
  remAddr.sin_addr   = ipAddr;

  struct msghdr msg;
  struct iovec  iov;
  iov.iov_base    = _frame;
  iov.iov_len     = size;
  memset(&msg, 0, sizeof(msg));
  msg.msg_name    = &remAddr;
  msg.msg_namelen = sizeof(remAddr);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;
  
  return rt_dev_sendmsg(_sock, &msg, 0);
}

int RtnodeCtr::_rec()
{
  struct msghdr      msg;
  struct iovec       iov;
  struct sockaddr_in addr;

  iov.iov_base = _frame;
  iov.iov_len  = defFrameSize;

  memset(&msg, 0, sizeof(msg));
  msg.msg_name    = &addr;
  msg.msg_namelen = sizeof(addr);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;

  return rt_dev_recvmsg(_sock, &msg, 0);
}

int RtnodeCtr::_prepareFrameCtr(uint8_t type, uint32_t prm)
{
  sMsgRTnodeBaseCtr_t msg;

  msg.id   = _id++;
  msg.type = type;
  msg.prm  = prm;
  return msgRTnodeBaseCtrSerialize(&msg, _frame, defFrameSize);
}

int RtnodeCtr::_prepareFrameCfgNew()
{
  int ret, size;

  size = ret = _prepareFrameCtr(MSGRTNODEBASECTRDEF_TYPE_CFGNEW, 0);
  if(ret <= 0) return ret;
  size += ret = 
    msgRTnodeBaseCfgSerialize(&_cfg, _frame + size, defFrameSize - size);
  if(ret <= 0) return ret;
  return size;
}

bool RtnodeCtr::_parseFrameCfgRep(unsigned int size)
{
  sMsgRTnodeBaseCfg_t msg;
  int                 len;

  // Control message has been already parsed by connect method so skip this 
  // part and parse configuration message 
  len = msgRTnodeBaseCfgDeSerialize(&_cfg, _frame + _ctrSize, size - _ctrSize);
  if(len <= 0) return false;
  // We have valid configuration now
  _isNew = true;
  return true;
}

int RtnodeCtr::_connect(std::string const   &ip, 
			unsigned int         size, 
			sMsgRTnodeBaseCtr_t &msg)
{
  int ret, len;
  
  ret = _send(ip, size);
  if(ret < 0) return ret;
  len = _rec();
  if(len <= 0) return len;
  ret = msgRTnodeBaseCtrDeSerialize(&msg, _frame, len);
  if(ret <= 0) return ret;
  if(_verbose) _printMsgCtr(msg);
  return len;
}

bool RtnodeCtr::_ping(std::string const& ip)
{
  if(!isActive()){
    std::cout << "Start first!" << std::endl;
    return false;
  }
  // Store message id
  uint8_t id = _id;
  // Prepare message 
  int size;
  size = _prepareFrameCtr(MSGRTNODEBASECTRDEF_TYPE_PING, 0);
  if(size <= 0) return false;
  // Send frame and acquire replay 
  sMsgRTnodeBaseCtr_t msg;
  int                 ret;
  ret = _connect(ip, size, msg);
  if(ret <= 0){
    std::cout << conerr2str(ret) << std::endl;
    return false;
  }
  // Validate message 
  if(msg.id   != id){
    std::cout << "Node replay with wrong message id = " << msg.id 
	      << ", should be "<< id << "!" << std::endl;
    return false;
  }
  if(msg.type != MSGRTNODEBASECTRDEF_TYPE_ERR){
    std::cout << "Node replay with wrong message type = " << type2str(msg.type)
	      << ", should be "
	      << type2str(MSGRTNODEBASECTRDEF_TYPE_ERR) << "!" << std::endl;
    return false;
  }
  if(msg.prm == MSGRTNODEBASECTRDEF_ERR_OK)
    std::cout << err2str(msg.prm) << std::endl;
  else
    std::cout << "Node report error: " << err2str(msg.prm) << std::endl;
  return true;
}

bool RtnodeCtr::_reboot(std::string const& ip, unsigned int delay)
{
  if(!isActive()){
    std::cout << "Start first!" << std::endl;
    return false;
  }
  // Prepare message 
  int size;
  size = _prepareFrameCtr(MSGRTNODEBASECTRDEF_TYPE_REBOOT, delay);
  if(size <= 0) return false;
  // Send frame and no wait for response
  int ret;
  ret = _send(ip, size);
  if(ret <= 0){
    std::cout << conerr2str(ret) << std::endl;
    return false;
  }
  return true;
}

bool RtnodeCtr::_cfgNew(std::string const& ip)
{
  if(!isActive()){
    std::cout << "Start first!" << std::endl;
    return false;
  }
  if(!_isNew){
    std::cout << "Set configuration structure first!" << std::endl;
    return false;
  }
  // Store message id
  uint8_t id = _id;
  // Prepare message 
  int size;
  size = _prepareFrameCfgNew();
  if(size <= 0) return false;
  // Send frame and acquire replay 
  sMsgRTnodeBaseCtr_t msg;
  int                 ret;
  ret = _connect(ip, size, msg);
  if(ret <= 0){
    std::cout << conerr2str(ret) << std::endl;
    return false;
  }
  // Validate message 
  if(msg.id != id){
    std::cout << "Node replay with wrong message id = " << msg.id 
	      << ", should be "<< id << "!" << std::endl;
    return false;
  }
  if(msg.type != MSGRTNODEBASECTRDEF_TYPE_ERR){
    std::cout << "Node replay with wrong message type = " << type2str(msg.type)
	      << ", should be "
	      << type2str(MSGRTNODEBASECTRDEF_TYPE_ERR) << "!" << std::endl;
    return false;
  }
  if(msg.prm == MSGRTNODEBASECTRDEF_ERR_OK)
    std::cout << err2str(msg.prm) << std::endl;
  else
    std::cout << "Node report error: " << err2str(msg.prm) << std::endl;
  return true;
}

bool RtnodeCtr::_cfgReq(std::string const& ip)
{
  if(!isActive()){
    std::cout << "Start first!" << std::endl;
    return false;
  }
  // Store message id
  uint8_t id = _id;
  // Prepare message 
  int size;
  size = _prepareFrameCtr(MSGRTNODEBASECTRDEF_TYPE_CFGREQ, 0);
  if(size <= 0) return false;
  // Send frame and acquire replay 
  sMsgRTnodeBaseCtr_t msg;
  int                 ret;
  ret = _connect(ip, size, msg);
  if(ret <= 0){
    std::cout << conerr2str(ret) << std::endl;
    return false;
  }
  // Validate message 
  if(msg.id   != id){
    std::cout << "Node replay with wrong message id = " << msg.id 
	      << ", should be "<< id << "!" << std::endl;
    return false;
  }
  switch(msg.type){
  case MSGRTNODEBASECTRDEF_TYPE_ERR:
    std::cout << "Node report error: " << err2str(msg.prm) << std::endl;
    return false;
  case MSGRTNODEBASECTRDEF_TYPE_CFGREP:

    break;
  default:
    std::cout << "Node replay with wrong message type = " << type2str(msg.type)
	      << ", should be "
	      << type2str(MSGRTNODEBASECTRDEF_TYPE_ERR) << "!" << std::endl;
    return false;
  }
  // Get node configuration from frame 
  return _parseFrameCfgRep(ret);
}

const std::string RtnodeCtr::conerr2str(int err)
{
  switch(err){
  case -ETIMEDOUT:
    return std::string("Connection timeout!");
  default:
    return 
      std::string("Connection error (")     +
      boost::lexical_cast<std::string>(err) +
      std::string(")!");
  }
}

const std::string RtnodeCtr::err2str(uint8_t err)
{
  switch(err){
    case MSGRTNODEBASECTRDEF_ERR_OK:
      return std::string("OK");
    case MSGRTNODEBASECTRDEF_ERR_UNKNOWN:
      return std::string("UNKNOWN");
    case MSGRTNODEBASECTRDEF_ERR_MEM:
      return std::string("MEM");
    case MSGRTNODEBASECTRDEF_ERR_FRAME:
      return std::string("FRAME");
    case MSGRTNODEBASECTRDEF_ERR_FLASH:
      return std::string("FLASH");
    case MSGRTNODEBASECTRDEF_ERR_INT:
      return std::string("INT");
    default:
      return boost::lexical_cast<std::string>(err);
    }
}

const std::string RtnodeCtr::type2str(uint8_t type)
{
  switch(type){
  case MSGRTNODEBASECTRDEF_TYPE_ERR:
    return std::string("ERR");
  case MSGRTNODEBASECTRDEF_TYPE_PING:
    return std::string("PING");
  case MSGRTNODEBASECTRDEF_TYPE_REBOOT:
    return std::string("REBOOT");
  case MSGRTNODEBASECTRDEF_TYPE_CFGNEW:
    return std::string("CFGNEW");
  case MSGRTNODEBASECTRDEF_TYPE_CFGREQ:
    return std::string("CFGREQ");
  case MSGRTNODEBASECTRDEF_TYPE_CFGREP:
    return std::string("CFGREP");
  default:
    return boost::lexical_cast<std::string>(type);
  }
}


void RtnodeCtr::_printMsgCtr(sMsgRTnodeBaseCtr_t const& msg)
{
  std::cout << "---------------------------"   << std::endl;
  std::cout << "id   = " << (int) msg.id       << std::endl;
  std::cout << "type = " << type2str(msg.type) << std::endl;
  switch(msg.type){
  case MSGRTNODEBASECTRDEF_TYPE_ERR:
    std::cout << "prm  = " << err2str(msg.prm) << std::endl;
    break;
  default:
    std::cout << "prm  = " << (int) msg.prm    << std::endl;
  }
  std::cout << "---------------------------" << std::endl;
}

void RtnodeCtr::_printCfg()
{
  std::cout << "---------------------------" << std::endl;
  std::cout << "id          = " << (int)_cfg.id          << std::endl;
  std::cout << "gidLen      = " << (int)_cfg.gidLen      << std::endl;
  std::cout << "gid0        = " << (int)_cfg.gid0        << std::endl;
  std::cout << "gid1        = " << (int)_cfg.gid1        << std::endl;
  std::cout << "gid2        = " << (int)_cfg.gid2        << std::endl;
  std::cout << "gid3        = " << (int)_cfg.gid3        << std::endl;
  std::cout << "gid4        = " << (int)_cfg.gid4        << std::endl;
  std::cout << "gid5        = " << (int)_cfg.gid5        << std::endl;
  std::cout << "gid6        = " << (int)_cfg.gid6        << std::endl;
  std::cout << "gid7        = " << (int)_cfg.gid7        << std::endl;
  std::cout << "gid8        = " << (int)_cfg.gid8        << std::endl;
  std::cout << "gid9        = " << (int)_cfg.gid9        << std::endl; 
  std::cout << "ethMac0     = " << (int)_cfg.ethMac0     << std::endl;
  std::cout << "ethMac1     = " << (int)_cfg.ethMac1     << std::endl;
  std::cout << "ethMac2     = " << (int)_cfg.ethMac2     << std::endl;
  std::cout << "ethMac3     = " << (int)_cfg.ethMac3     << std::endl;
  std::cout << "ethMac4     = " << (int)_cfg.ethMac4     << std::endl;
  std::cout << "ethMac5     = " << (int)_cfg.ethMac5     << std::endl;
  std::cout << "ethCtrPort  = " << (int)_cfg.ethCtrPort  << std::endl;
  std::cout << "ethRecPort  = " << (int)_cfg.ethRecPort  << std::endl;
  std::cout << "ethSndPort  = " << (int)_cfg.ethSndPort  << std::endl;
  std::cout << "ethRemPort  = " << (int)_cfg.ethRemPort  << std::endl;
  std::cout << "ethDelay    = " << (int)_cfg.ethDelay    << std::endl;
  std::cout << "updateFreq  = " << (int)_cfg.updateFreq  << std::endl;
  std::cout << "qei0VelFreq = " << (int)_cfg.qei0VelFreq << std::endl;
  std::cout << "qei0VelExt  = " << (int)_cfg.qei0VelExt  << std::endl;
  std::cout << "qei1VelFreq = " << (int)_cfg.qei1VelFreq << std::endl;
  std::cout << "qei1VelExt  = " << (int)_cfg.qei1VelExt  << std::endl;
  std::cout << "---------------------------" << std::endl;
}


/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Rtnode)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(RtnodeCtr)
