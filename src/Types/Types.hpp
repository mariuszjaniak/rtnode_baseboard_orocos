/* Generated from orogen/lib/orogen/templates/typekit/Types.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPES_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPES_HPP





#include "msgRTnodeBase_typekit/types/msgRTnodeBase_typekit/Msg/msgRTnodeBaseIn.h"

#include "msgRTnodeBase_typekit/types/msgRTnodeBase_typekit/Msg/msgRTnodeBaseOut.h"

#include "msgRTnodeBase_typekit/types/msgRTnodeBase_typekit/Msg/msgRTnodeBaseCfg.h"

#include "msgRTnodeBase_typekit/types/msgRTnodeBase_typekit/Msg/msgRTnodeBaseCtr.h"




// This is a hack. We include it unconditionally as it may be required by some
// typekits *and* it is a standard header. Ideally, we would actually check if
// some of the types need std::vector.
#include <vector>
#include <boost/cstdint.hpp>




#ifdef ORO_CHANNEL_ELEMENT_HPP
    extern template class RTT::base::ChannelElement< sMSGRTNODEBASECFG >;
#endif
#ifdef CORELIB_DATASOURCE_HPP
    extern template class RTT::internal::DataSource< sMSGRTNODEBASECFG >;
    extern template class RTT::internal::AssignableDataSource< sMSGRTNODEBASECFG >;
#endif
#ifdef ORO_CORELIB_DATASOURCES_HPP
    extern template class RTT::internal::ValueDataSource< sMSGRTNODEBASECFG >;
    extern template class RTT::internal::ConstantDataSource< sMSGRTNODEBASECFG >;
    extern template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASECFG >;
#endif
#ifdef ORO_INPUT_PORT_HPP
    extern template class RTT::OutputPort< sMSGRTNODEBASECFG >;
#endif
#ifdef ORO_OUTPUT_PORT_HPP
    extern template class RTT::InputPort< sMSGRTNODEBASECFG >;
#endif
#ifdef ORO_PROPERTY_HPP
    extern template class RTT::Property< sMSGRTNODEBASECFG >;
#endif
#ifdef ORO_CORELIB_ATTRIBUTE_HPP
    extern template class RTT::Attribute< sMSGRTNODEBASECFG >;
#endif

#ifdef ORO_CHANNEL_ELEMENT_HPP
    extern template class RTT::base::ChannelElement< sMSGRTNODEBASECTR >;
#endif
#ifdef CORELIB_DATASOURCE_HPP
    extern template class RTT::internal::DataSource< sMSGRTNODEBASECTR >;
    extern template class RTT::internal::AssignableDataSource< sMSGRTNODEBASECTR >;
#endif
#ifdef ORO_CORELIB_DATASOURCES_HPP
    extern template class RTT::internal::ValueDataSource< sMSGRTNODEBASECTR >;
    extern template class RTT::internal::ConstantDataSource< sMSGRTNODEBASECTR >;
    extern template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASECTR >;
#endif
#ifdef ORO_INPUT_PORT_HPP
    extern template class RTT::OutputPort< sMSGRTNODEBASECTR >;
#endif
#ifdef ORO_OUTPUT_PORT_HPP
    extern template class RTT::InputPort< sMSGRTNODEBASECTR >;
#endif
#ifdef ORO_PROPERTY_HPP
    extern template class RTT::Property< sMSGRTNODEBASECTR >;
#endif
#ifdef ORO_CORELIB_ATTRIBUTE_HPP
    extern template class RTT::Attribute< sMSGRTNODEBASECTR >;
#endif

#ifdef ORO_CHANNEL_ELEMENT_HPP
    extern template class RTT::base::ChannelElement< sMSGRTNODEBASEIN >;
#endif
#ifdef CORELIB_DATASOURCE_HPP
    extern template class RTT::internal::DataSource< sMSGRTNODEBASEIN >;
    extern template class RTT::internal::AssignableDataSource< sMSGRTNODEBASEIN >;
#endif
#ifdef ORO_CORELIB_DATASOURCES_HPP
    extern template class RTT::internal::ValueDataSource< sMSGRTNODEBASEIN >;
    extern template class RTT::internal::ConstantDataSource< sMSGRTNODEBASEIN >;
    extern template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASEIN >;
#endif
#ifdef ORO_INPUT_PORT_HPP
    extern template class RTT::OutputPort< sMSGRTNODEBASEIN >;
#endif
#ifdef ORO_OUTPUT_PORT_HPP
    extern template class RTT::InputPort< sMSGRTNODEBASEIN >;
#endif
#ifdef ORO_PROPERTY_HPP
    extern template class RTT::Property< sMSGRTNODEBASEIN >;
#endif
#ifdef ORO_CORELIB_ATTRIBUTE_HPP
    extern template class RTT::Attribute< sMSGRTNODEBASEIN >;
#endif

#ifdef ORO_CHANNEL_ELEMENT_HPP
    extern template class RTT::base::ChannelElement< sMSGRTNODEBASEOUT >;
#endif
#ifdef CORELIB_DATASOURCE_HPP
    extern template class RTT::internal::DataSource< sMSGRTNODEBASEOUT >;
    extern template class RTT::internal::AssignableDataSource< sMSGRTNODEBASEOUT >;
#endif
#ifdef ORO_CORELIB_DATASOURCES_HPP
    extern template class RTT::internal::ValueDataSource< sMSGRTNODEBASEOUT >;
    extern template class RTT::internal::ConstantDataSource< sMSGRTNODEBASEOUT >;
    extern template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASEOUT >;
#endif
#ifdef ORO_INPUT_PORT_HPP
    extern template class RTT::OutputPort< sMSGRTNODEBASEOUT >;
#endif
#ifdef ORO_OUTPUT_PORT_HPP
    extern template class RTT::InputPort< sMSGRTNODEBASEOUT >;
#endif
#ifdef ORO_PROPERTY_HPP
    extern template class RTT::Property< sMSGRTNODEBASEOUT >;
#endif
#ifdef ORO_CORELIB_ATTRIBUTE_HPP
    extern template class RTT::Attribute< sMSGRTNODEBASEOUT >;
#endif

#ifdef ORO_CHANNEL_ELEMENT_HPP
    extern template class RTT::base::ChannelElement< ::std::string >;
#endif
#ifdef CORELIB_DATASOURCE_HPP
    extern template class RTT::internal::DataSource< ::std::string >;
    extern template class RTT::internal::AssignableDataSource< ::std::string >;
#endif
#ifdef ORO_CORELIB_DATASOURCES_HPP
    extern template class RTT::internal::ValueDataSource< ::std::string >;
    extern template class RTT::internal::ConstantDataSource< ::std::string >;
    extern template class RTT::internal::ReferenceDataSource< ::std::string >;
#endif
#ifdef ORO_INPUT_PORT_HPP
    extern template class RTT::OutputPort< ::std::string >;
#endif
#ifdef ORO_OUTPUT_PORT_HPP
    extern template class RTT::InputPort< ::std::string >;
#endif
#ifdef ORO_PROPERTY_HPP
    extern template class RTT::Property< ::std::string >;
#endif
#ifdef ORO_CORELIB_ATTRIBUTE_HPP
    extern template class RTT::Attribute< ::std::string >;
#endif


#endif

