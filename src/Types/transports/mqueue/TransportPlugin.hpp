/* Generated from orogen/lib/orogen/templates/typekit/mqueue/TransportPlugin.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_MQUEUE_PLUGIN_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_MQUEUE_PLUGIN_HPP

#include <rtt/types/TransportPlugin.hpp>

namespace Typelib {
    class Registry;
}
namespace orogen_typekits {
    class msgRTnodeBase_typekitMQueueTransportPlugin
        : public RTT::types::TransportPlugin
    {
        Typelib::Registry* m_registry;

    public:
        msgRTnodeBase_typekitMQueueTransportPlugin();
        ~msgRTnodeBase_typekitMQueueTransportPlugin();

        virtual bool registerTransport(std::string type_name, RTT::types::TypeInfo* ti);
        virtual std::string getTransportName() const;
        virtual std::string getTypekitName() const;
        virtual std::string getName() const;
    };

    extern msgRTnodeBase_typekitMQueueTransportPlugin msgRTnodeBase_typekitMQueueTransport;
}

#endif

