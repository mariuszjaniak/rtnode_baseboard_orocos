/* Generated from orogen/lib/orogen/templates/typekit/mqueue/Registration.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_MQUEUE_REGISTRATION_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_MQUEUE_REGISTRATION_HPP

#include <rtt/types/TypeMarshaller.hpp>

namespace Typelib {
    class Registry;
}
namespace orogen_typekits {
    
    RTT::types::TypeMarshaller*  sMSGRTNODEBASECFG_MQueueTransport(Typelib::Registry const& registry);
    
    RTT::types::TypeMarshaller*  sMSGRTNODEBASECTR_MQueueTransport(Typelib::Registry const& registry);
    
    RTT::types::TypeMarshaller*  sMSGRTNODEBASEIN_MQueueTransport(Typelib::Registry const& registry);
    
    RTT::types::TypeMarshaller*  sMSGRTNODEBASEOUT_MQueueTransport(Typelib::Registry const& registry);
    
    RTT::types::TypeMarshaller*  std_string_MQueueTransport(Typelib::Registry const& registry);
    
}

#endif


