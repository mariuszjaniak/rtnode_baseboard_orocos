/* Generated from orogen/lib/orogen/templates/typekit/mqueue/Type.cpp */

#include "Types.hpp"
#include "transports/mqueue/Registration.hpp"



#include <rtt/transports/mqueue/MQTemplateProtocol.hpp>


namespace Typelib
{
    class Registry;
}

namespace orogen_typekits {
    RTT::types::TypeMarshaller*  sMSGRTNODEBASECTR_MQueueTransport(Typelib::Registry const& registry)
    {
        
        return new RTT::mqueue::MQTemplateProtocol< sMSGRTNODEBASECTR >();
        
    }
}


/* Generated from orogen/lib/orogen/templates/typekit/mqueue/Type.cpp */

#include "Types.hpp"
#include "transports/mqueue/Registration.hpp"



#include <rtt/transports/mqueue/MQTemplateProtocol.hpp>


namespace Typelib
{
    class Registry;
}

namespace orogen_typekits {
    RTT::types::TypeMarshaller*  sMSGRTNODEBASEIN_MQueueTransport(Typelib::Registry const& registry)
    {
        
        return new RTT::mqueue::MQTemplateProtocol< sMSGRTNODEBASEIN >();
        
    }
}

