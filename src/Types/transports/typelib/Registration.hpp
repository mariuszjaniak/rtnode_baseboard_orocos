/* Generated from orogen/lib/orogen/templates/typekit/typelib/Registration.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPELIB_REGISTRATION_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPELIB_REGISTRATION_HPP

#include <rtt/types/TypeInfoGenerator.hpp>

namespace Typelib {
    class Registry;
}

namespace orogen_transports {
    class TypelibMarshallerBase;
}

namespace orogen_typekits {
    
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASECFG */
    orogen_transports::TypelibMarshallerBase* sMSGRTNODEBASECFG_TypelibMarshaller(Typelib::Registry const& registry);
    
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASECTR */
    orogen_transports::TypelibMarshallerBase* sMSGRTNODEBASECTR_TypelibMarshaller(Typelib::Registry const& registry);
    
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASEIN */
    orogen_transports::TypelibMarshallerBase* sMSGRTNODEBASEIN_TypelibMarshaller(Typelib::Registry const& registry);
    
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASEOUT */
    orogen_transports::TypelibMarshallerBase* sMSGRTNODEBASEOUT_TypelibMarshaller(Typelib::Registry const& registry);
    
    /** Creates and returns a TypeInfo object for ::std::string */
    orogen_transports::TypelibMarshallerBase* std_string_TypelibMarshaller(Typelib::Registry const& registry);
    
}

#endif


