/* Generated from orogen/lib/orogen/templates/typekit/corba/TransportPlugin.cpp */

// First load all RTT interfaces so that we get all "extern template"
// declarations in the TypekitImpl files
#include "transports/corba/Registration.hpp"
#include "transports/corba/TransportPlugin.hpp"
#include <rtt/transports/corba/CorbaLib.hpp>
#include <rtt/types/TypekitPlugin.hpp>
using namespace RTT;

bool orogen_typekits::msgRTnodeBase_typekitCorbaTransportPlugin::registerTransport(std::string type_name, RTT::types::TypeInfo* ti)
{
    if(ti->hasProtocol(ORO_CORBA_PROTOCOL_ID))
	return false;

    
    if ("/sMSGRTNODEBASECFG" == type_name)
    {
        return ti->addProtocol(ORO_CORBA_PROTOCOL_ID,
            sMSGRTNODEBASECFG_CorbaTransport());
    }
    
    else if ("/sMSGRTNODEBASECTR" == type_name)
    {
        return ti->addProtocol(ORO_CORBA_PROTOCOL_ID,
            sMSGRTNODEBASECTR_CorbaTransport());
    }
    
    else if ("/sMSGRTNODEBASEIN" == type_name)
    {
        return ti->addProtocol(ORO_CORBA_PROTOCOL_ID,
            sMSGRTNODEBASEIN_CorbaTransport());
    }
    
    else if ("/sMSGRTNODEBASEOUT" == type_name)
    {
        return ti->addProtocol(ORO_CORBA_PROTOCOL_ID,
            sMSGRTNODEBASEOUT_CorbaTransport());
    }
    
    else if ("/std/string" == type_name)
    {
        return ti->addProtocol(ORO_CORBA_PROTOCOL_ID,
            std_string_CorbaTransport());
    }
    
    return false;
}
std::string orogen_typekits::msgRTnodeBase_typekitCorbaTransportPlugin::getTransportName() const
{ return "CORBA"; }
std::string orogen_typekits::msgRTnodeBase_typekitCorbaTransportPlugin::getTypekitName() const
{ return "/orogen/msgRTnodeBase_typekit"; }
std::string orogen_typekits::msgRTnodeBase_typekitCorbaTransportPlugin::getName() const
{ return "/orogen/msgRTnodeBase_typekit/CORBA"; }

ORO_TYPEKIT_PLUGIN(orogen_typekits::msgRTnodeBase_typekitCorbaTransportPlugin);

