/* Generated from orogen/lib/orogen/templates/typekit/corba/Registration.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_CORBA_REGISTRATION_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_CORBA_REGISTRATION_HPP

#include <rtt/transports/corba/CorbaTypeTransporter.hpp>

namespace orogen_typekits {
    
    RTT::corba::CorbaTypeTransporter*  sMSGRTNODEBASECFG_CorbaTransport();
    
    RTT::corba::CorbaTypeTransporter*  sMSGRTNODEBASECTR_CorbaTransport();
    
    RTT::corba::CorbaTypeTransporter*  sMSGRTNODEBASEIN_CorbaTransport();
    
    RTT::corba::CorbaTypeTransporter*  sMSGRTNODEBASEOUT_CorbaTransport();
    
    RTT::corba::CorbaTypeTransporter*  std_string_CorbaTransport();
    
}

#endif


