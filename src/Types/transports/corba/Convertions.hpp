/* Generated from orogen/lib/orogen/templates/typekit/corba/Convertions.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_CORBA_CONVERTIONS_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_CORBA_CONVERTIONS_HPP

#include "../../Types.hpp"
#include "msgRTnodeBase_typekit/transports/corba/msgRTnodeBase_typekitTypesC.h"
#include <boost/cstdint.hpp>
#include <string>

namespace orogen_typekits {
    /** Converted types: */
    
    bool toCORBA( orogen::Corba::sMSGRTNODEBASECFG& corba, sMSGRTNODEBASECFG const& value );
    bool fromCORBA( sMSGRTNODEBASECFG& value, orogen::Corba::sMSGRTNODEBASECFG const& corba );
    
    bool toCORBA( orogen::Corba::sMSGRTNODEBASECTR& corba, sMSGRTNODEBASECTR const& value );
    bool fromCORBA( sMSGRTNODEBASECTR& value, orogen::Corba::sMSGRTNODEBASECTR const& corba );
    
    bool toCORBA( orogen::Corba::sMSGRTNODEBASEIN& corba, sMSGRTNODEBASEIN const& value );
    bool fromCORBA( sMSGRTNODEBASEIN& value, orogen::Corba::sMSGRTNODEBASEIN const& corba );
    
    bool toCORBA( orogen::Corba::sMSGRTNODEBASEOUT& corba, sMSGRTNODEBASEOUT const& value );
    bool fromCORBA( sMSGRTNODEBASEOUT& value, orogen::Corba::sMSGRTNODEBASEOUT const& corba );
    
    bool toCORBA( char const*& corba, ::std::string const& value );
    bool fromCORBA( ::std::string& value, char const* corba );
    
    /** Array types: */
    
}

#endif

