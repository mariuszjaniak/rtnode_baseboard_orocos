/* Generated from orogen/lib/orogen/templates/typekit/corba/Convertions.cpp */

#include "Convertions.hpp"
#include <memory>







bool orogen_typekits::toCORBA( orogen::Corba::sMSGRTNODEBASECFG& corba, sMSGRTNODEBASECFG const& value )
{
    corba.id = value.id;
    corba.gidLen = value.gidLen;
    corba.gid0 = value.gid0;
    corba.gid1 = value.gid1;
    corba.gid2 = value.gid2;
    corba.gid3 = value.gid3;
    corba.gid4 = value.gid4;
    corba.gid5 = value.gid5;
    corba.gid6 = value.gid6;
    corba.gid7 = value.gid7;
    corba.gid8 = value.gid8;
    corba.gid9 = value.gid9;
    corba.ethMac0 = value.ethMac0;
    corba.ethMac1 = value.ethMac1;
    corba.ethMac2 = value.ethMac2;
    corba.ethMac3 = value.ethMac3;
    corba.ethMac4 = value.ethMac4;
    corba.ethMac5 = value.ethMac5;
    corba.ethCtrPort = value.ethCtrPort;
    corba.ethRecPort = value.ethRecPort;
    corba.ethSndPort = value.ethSndPort;
    corba.ethRemPort = value.ethRemPort;
    corba.ethDelay = value.ethDelay;
    corba.updateFreq = value.updateFreq;
    corba.qei0VelFreq = value.qei0VelFreq;
    corba.qei0VelExt = value.qei0VelExt;
    corba.qei1VelFreq = value.qei1VelFreq;
    corba.qei1VelExt = value.qei1VelExt;

    return true;
}
bool orogen_typekits::fromCORBA( sMSGRTNODEBASECFG& value, orogen::Corba::sMSGRTNODEBASECFG const& corba )
{
    value.id = corba.id;
    value.gidLen = corba.gidLen;
    value.gid0 = corba.gid0;
    value.gid1 = corba.gid1;
    value.gid2 = corba.gid2;
    value.gid3 = corba.gid3;
    value.gid4 = corba.gid4;
    value.gid5 = corba.gid5;
    value.gid6 = corba.gid6;
    value.gid7 = corba.gid7;
    value.gid8 = corba.gid8;
    value.gid9 = corba.gid9;
    value.ethMac0 = corba.ethMac0;
    value.ethMac1 = corba.ethMac1;
    value.ethMac2 = corba.ethMac2;
    value.ethMac3 = corba.ethMac3;
    value.ethMac4 = corba.ethMac4;
    value.ethMac5 = corba.ethMac5;
    value.ethCtrPort = corba.ethCtrPort;
    value.ethRecPort = corba.ethRecPort;
    value.ethSndPort = corba.ethSndPort;
    value.ethRemPort = corba.ethRemPort;
    value.ethDelay = corba.ethDelay;
    value.updateFreq = corba.updateFreq;
    value.qei0VelFreq = corba.qei0VelFreq;
    value.qei0VelExt = corba.qei0VelExt;
    value.qei1VelFreq = corba.qei1VelFreq;
    value.qei1VelExt = corba.qei1VelExt;

    return true;
}

bool orogen_typekits::toCORBA( orogen::Corba::sMSGRTNODEBASECTR& corba, sMSGRTNODEBASECTR const& value )
{
    corba.id = value.id;
    corba.type = value.type;
    corba.prm = value.prm;

    return true;
}
bool orogen_typekits::fromCORBA( sMSGRTNODEBASECTR& value, orogen::Corba::sMSGRTNODEBASECTR const& corba )
{
    value.id = corba.id;
    value.type = corba.type;
    value.prm = corba.prm;

    return true;
}

bool orogen_typekits::toCORBA( orogen::Corba::sMSGRTNODEBASEIN& corba, sMSGRTNODEBASEIN const& value )
{
    corba.id = value.id;
    corba.gid = value.gid;
    corba.ch00 = value.ch00;
    corba.ch01 = value.ch01;
    corba.ch02 = value.ch02;
    corba.ch03 = value.ch03;
    corba.i0 = value.i0;
    corba.i1 = value.i1;
    corba.pos0 = value.pos0;
    corba.tic0 = value.tic0;
    corba.t0 = value.t0;
    corba.dir0 = value.dir0;
    corba.err0 = value.err0;
    corba.pos1 = value.pos1;
    corba.tic1 = value.tic1;
    corba.t1 = value.t1;
    corba.dir1 = value.dir1;
    corba.err1 = value.err1;

    return true;
}
bool orogen_typekits::fromCORBA( sMSGRTNODEBASEIN& value, orogen::Corba::sMSGRTNODEBASEIN const& corba )
{
    value.id = corba.id;
    value.gid = corba.gid;
    value.ch00 = corba.ch00;
    value.ch01 = corba.ch01;
    value.ch02 = corba.ch02;
    value.ch03 = corba.ch03;
    value.i0 = corba.i0;
    value.i1 = corba.i1;
    value.pos0 = corba.pos0;
    value.tic0 = corba.tic0;
    value.t0 = corba.t0;
    value.dir0 = corba.dir0;
    value.err0 = corba.err0;
    value.pos1 = corba.pos1;
    value.tic1 = corba.tic1;
    value.t1 = corba.t1;
    value.dir1 = corba.dir1;
    value.err1 = corba.err1;

    return true;
}

bool orogen_typekits::toCORBA( orogen::Corba::sMSGRTNODEBASEOUT& corba, sMSGRTNODEBASEOUT const& value )
{
    corba.id = value.id;
    corba.gid = value.gid;
    corba.ch00 = value.ch00;
    corba.ch01 = value.ch01;
    corba.o0 = value.o0;
    corba.o1 = value.o1;
    corba.mask = value.mask;

    return true;
}
bool orogen_typekits::fromCORBA( sMSGRTNODEBASEOUT& value, orogen::Corba::sMSGRTNODEBASEOUT const& corba )
{
    value.id = corba.id;
    value.gid = corba.gid;
    value.ch00 = corba.ch00;
    value.ch01 = corba.ch01;
    value.o0 = corba.o0;
    value.o1 = corba.o1;
    value.mask = corba.mask;

    return true;
}

bool orogen_typekits::toCORBA( char const*& corba, ::std::string const& value )
{
    corba = value.c_str();

    return true;
}
bool orogen_typekits::fromCORBA( ::std::string& value, char const* corba )
{
    value = corba;

    return true;
}



