/* Generated from orogen/lib/orogen/templates/typekit/Plugin.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPEKIT_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_TYPEKIT_HPP

#include <rtt/types/TypekitPlugin.hpp>

namespace Typelib {
    class Registry;
}

namespace orogen_typekits {
    class msgRTnodeBase_typekitTypekitPlugin
        : public RTT::types::TypekitPlugin
    {
        Typelib::Registry* m_registry;

    public:
        msgRTnodeBase_typekitTypekitPlugin();
        ~msgRTnodeBase_typekitTypekitPlugin();
        bool loadTypes();
        bool loadOperators();
        bool loadConstructors();
        std::string getName();
    };

    extern msgRTnodeBase_typekitTypekitPlugin msgRTnodeBase_typekitTypekit;
}

#endif


