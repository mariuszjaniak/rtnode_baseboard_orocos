/* Generated from orogen/templates/typekit/type_info/Info.cpp */

#include <msgRTnodeBase_typekit/Types.hpp>
#include <msgRTnodeBase_typekit/type_info/BoostSerialization.hpp>
#include <rtt/types/StructTypeInfo.hpp>




namespace orogen_typekits {
    struct sMSGRTNODEBASEOUTTypeInfo :
        public RTT::types::StructTypeInfo< sMSGRTNODEBASEOUT >
    {
        sMSGRTNODEBASEOUTTypeInfo()
            : RTT::types::StructTypeInfo< sMSGRTNODEBASEOUT >("/sMSGRTNODEBASEOUT") {}



    };

    RTT::types::TypeInfoGenerator* sMSGRTNODEBASEOUT_TypeInfo()
    { return new sMSGRTNODEBASEOUTTypeInfo(); }

}

/* Generated from orogen/lib/orogen/templates/typekit/TemplateInstanciation.cpp */

#include <rtt/Port.hpp>
#include <rtt/Attribute.hpp>
#include <rtt/Property.hpp>
#include <rtt/internal/DataSource.hpp>

template class RTT::OutputPort< sMSGRTNODEBASEOUT >;
template class RTT::InputPort< sMSGRTNODEBASEOUT >;
template class RTT::Property< sMSGRTNODEBASEOUT >;
template class RTT::Attribute< sMSGRTNODEBASEOUT >;

template class RTT::internal::DataSource< sMSGRTNODEBASEOUT >;
template class RTT::internal::ValueDataSource< sMSGRTNODEBASEOUT >;
template class RTT::internal::ConstantDataSource< sMSGRTNODEBASEOUT >;
template class RTT::internal::AssignableDataSource< sMSGRTNODEBASEOUT >;
template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASEOUT >;



