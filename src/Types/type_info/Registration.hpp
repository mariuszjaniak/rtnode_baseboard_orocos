/* Generated from orogen/lib/orogen/templates/typekit/type_info/TypeInfo.hpp */

#ifndef MSGRTNODEBASE_TYPEKIT_TYPE_INFO_HPP
#define MSGRTNODEBASE_TYPEKIT_TYPE_INFO_HPP

namespace orogen_typekits {
    
        
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASECFG */
    RTT::types::TypeInfoGenerator* sMSGRTNODEBASECFG_TypeInfo();
        
    
        
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASECTR */
    RTT::types::TypeInfoGenerator* sMSGRTNODEBASECTR_TypeInfo();
        
    
        
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASEIN */
    RTT::types::TypeInfoGenerator* sMSGRTNODEBASEIN_TypeInfo();
        
    
        
    /** Creates and returns a TypeInfo object for sMSGRTNODEBASEOUT */
    RTT::types::TypeInfoGenerator* sMSGRTNODEBASEOUT_TypeInfo();
        
    
        
    /** Creates and returns a TypeInfo object for ::std::string */
    RTT::types::TypeInfoGenerator* std_string_TypeInfo();
        
    
}

#endif

