/* Generated from orogen/templates/typekit/type_info/Info.cpp */

#include <msgRTnodeBase_typekit/Types.hpp>
#include <msgRTnodeBase_typekit/type_info/BoostSerialization.hpp>
#include <rtt/types/StructTypeInfo.hpp>




namespace orogen_typekits {
    struct sMSGRTNODEBASECTRTypeInfo :
        public RTT::types::StructTypeInfo< sMSGRTNODEBASECTR >
    {
        sMSGRTNODEBASECTRTypeInfo()
            : RTT::types::StructTypeInfo< sMSGRTNODEBASECTR >("/sMSGRTNODEBASECTR") {}



    };

    RTT::types::TypeInfoGenerator* sMSGRTNODEBASECTR_TypeInfo()
    { return new sMSGRTNODEBASECTRTypeInfo(); }

}

/* Generated from orogen/lib/orogen/templates/typekit/TemplateInstanciation.cpp */

#include <rtt/Port.hpp>
#include <rtt/Attribute.hpp>
#include <rtt/Property.hpp>
#include <rtt/internal/DataSource.hpp>

template class RTT::OutputPort< sMSGRTNODEBASECTR >;
template class RTT::InputPort< sMSGRTNODEBASECTR >;
template class RTT::Property< sMSGRTNODEBASECTR >;
template class RTT::Attribute< sMSGRTNODEBASECTR >;

template class RTT::internal::DataSource< sMSGRTNODEBASECTR >;
template class RTT::internal::ValueDataSource< sMSGRTNODEBASECTR >;
template class RTT::internal::ConstantDataSource< sMSGRTNODEBASECTR >;
template class RTT::internal::AssignableDataSource< sMSGRTNODEBASECTR >;
template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASECTR >;




/* Generated from orogen/templates/typekit/type_info/Info.cpp */

#include <msgRTnodeBase_typekit/Types.hpp>
#include <msgRTnodeBase_typekit/type_info/BoostSerialization.hpp>
#include <rtt/types/StructTypeInfo.hpp>




namespace orogen_typekits {
    struct sMSGRTNODEBASEINTypeInfo :
        public RTT::types::StructTypeInfo< sMSGRTNODEBASEIN >
    {
        sMSGRTNODEBASEINTypeInfo()
            : RTT::types::StructTypeInfo< sMSGRTNODEBASEIN >("/sMSGRTNODEBASEIN") {}



    };

    RTT::types::TypeInfoGenerator* sMSGRTNODEBASEIN_TypeInfo()
    { return new sMSGRTNODEBASEINTypeInfo(); }

}

/* Generated from orogen/lib/orogen/templates/typekit/TemplateInstanciation.cpp */

#include <rtt/Port.hpp>
#include <rtt/Attribute.hpp>
#include <rtt/Property.hpp>
#include <rtt/internal/DataSource.hpp>

template class RTT::OutputPort< sMSGRTNODEBASEIN >;
template class RTT::InputPort< sMSGRTNODEBASEIN >;
template class RTT::Property< sMSGRTNODEBASEIN >;
template class RTT::Attribute< sMSGRTNODEBASEIN >;

template class RTT::internal::DataSource< sMSGRTNODEBASEIN >;
template class RTT::internal::ValueDataSource< sMSGRTNODEBASEIN >;
template class RTT::internal::ConstantDataSource< sMSGRTNODEBASEIN >;
template class RTT::internal::AssignableDataSource< sMSGRTNODEBASEIN >;
template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASEIN >;



