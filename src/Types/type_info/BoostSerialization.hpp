/* Generated from orogen/lib/orogen/templates/typekit/BoostSerialization.hpp */

#ifndef __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_BOOST_SERIALIZATION_HPP
#define __OROGEN_GENERATED_MSGRTNODEBASE_TYPEKIT_BOOST_SERIALIZATION_HPP

#include <msgRTnodeBase_typekit/Types.hpp>

#include <boost/cstdint.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/serialization/utility.hpp>


namespace boost
{
    namespace serialization
    {

        template<typename Archive>
        void serialize(Archive& a, sMSGRTNODEBASECFG& b, unsigned int version)
        {
            using boost::serialization::make_nvp;
            a & make_nvp("id", b.id);
a & make_nvp("gidLen", b.gidLen);
a & make_nvp("gid0", b.gid0);
a & make_nvp("gid1", b.gid1);
a & make_nvp("gid2", b.gid2);
a & make_nvp("gid3", b.gid3);
a & make_nvp("gid4", b.gid4);
a & make_nvp("gid5", b.gid5);
a & make_nvp("gid6", b.gid6);
a & make_nvp("gid7", b.gid7);
a & make_nvp("gid8", b.gid8);
a & make_nvp("gid9", b.gid9);
a & make_nvp("ethMac0", b.ethMac0);
a & make_nvp("ethMac1", b.ethMac1);
a & make_nvp("ethMac2", b.ethMac2);
a & make_nvp("ethMac3", b.ethMac3);
a & make_nvp("ethMac4", b.ethMac4);
a & make_nvp("ethMac5", b.ethMac5);
a & make_nvp("ethCtrPort", b.ethCtrPort);
a & make_nvp("ethRecPort", b.ethRecPort);
a & make_nvp("ethSndPort", b.ethSndPort);
a & make_nvp("ethRemPort", b.ethRemPort);
a & make_nvp("ethDelay", b.ethDelay);
a & make_nvp("updateFreq", b.updateFreq);
a & make_nvp("qei0VelFreq", b.qei0VelFreq);
a & make_nvp("qei0VelExt", b.qei0VelExt);
a & make_nvp("qei1VelFreq", b.qei1VelFreq);
a & make_nvp("qei1VelExt", b.qei1VelExt);
        }

        template<typename Archive>
        void serialize(Archive& a, sMSGRTNODEBASECTR& b, unsigned int version)
        {
            using boost::serialization::make_nvp;
            a & make_nvp("id", b.id);
a & make_nvp("type", b.type);
a & make_nvp("prm", b.prm);
        }

        template<typename Archive>
        void serialize(Archive& a, sMSGRTNODEBASEIN& b, unsigned int version)
        {
            using boost::serialization::make_nvp;
            a & make_nvp("id", b.id);
a & make_nvp("gid", b.gid);
a & make_nvp("ch00", b.ch00);
a & make_nvp("ch01", b.ch01);
a & make_nvp("ch02", b.ch02);
a & make_nvp("ch03", b.ch03);
a & make_nvp("i0", b.i0);
a & make_nvp("i1", b.i1);
a & make_nvp("pos0", b.pos0);
a & make_nvp("tic0", b.tic0);
a & make_nvp("t0", b.t0);
a & make_nvp("dir0", b.dir0);
a & make_nvp("err0", b.err0);
a & make_nvp("pos1", b.pos1);
a & make_nvp("tic1", b.tic1);
a & make_nvp("t1", b.t1);
a & make_nvp("dir1", b.dir1);
a & make_nvp("err1", b.err1);
        }

        template<typename Archive>
        void serialize(Archive& a, sMSGRTNODEBASEOUT& b, unsigned int version)
        {
            using boost::serialization::make_nvp;
            a & make_nvp("id", b.id);
a & make_nvp("gid", b.gid);
a & make_nvp("ch00", b.ch00);
a & make_nvp("ch01", b.ch01);
a & make_nvp("o0", b.o0);
a & make_nvp("o1", b.o1);
a & make_nvp("mask", b.mask);
        }

    }
}


#endif

