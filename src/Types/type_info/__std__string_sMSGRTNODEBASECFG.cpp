/* Generated from orogen/templates/typekit/type_info/Info.cpp */

#include <msgRTnodeBase_typekit/Types.hpp>
#include <msgRTnodeBase_typekit/type_info/BoostSerialization.hpp>
#include <rtt/types/SequenceTypeInfo.hpp>

#include <rtt/typekit/StdStringTypeInfo.hpp>




namespace orogen_typekits {
    struct std_stringTypeInfo :
        public RTT::types::StdStringTypeInfo
    {
        std_stringTypeInfo()
            : RTT::types::StdStringTypeInfo("/std/string") {}



    };

    RTT::types::TypeInfoGenerator* std_string_TypeInfo()
    { return new std_stringTypeInfo(); }

}

/* Generated from orogen/lib/orogen/templates/typekit/TemplateInstanciation.cpp */

#include <rtt/Port.hpp>
#include <rtt/Attribute.hpp>
#include <rtt/Property.hpp>
#include <rtt/internal/DataSource.hpp>

template class RTT::OutputPort< ::std::string >;
template class RTT::InputPort< ::std::string >;
template class RTT::Property< ::std::string >;
template class RTT::Attribute< ::std::string >;

template class RTT::internal::DataSource< ::std::string >;
template class RTT::internal::ValueDataSource< ::std::string >;
template class RTT::internal::ConstantDataSource< ::std::string >;
template class RTT::internal::AssignableDataSource< ::std::string >;
template class RTT::internal::ReferenceDataSource< ::std::string >;




/* Generated from orogen/templates/typekit/type_info/Info.cpp */

#include <msgRTnodeBase_typekit/Types.hpp>
#include <msgRTnodeBase_typekit/type_info/BoostSerialization.hpp>
#include <rtt/types/StructTypeInfo.hpp>




namespace orogen_typekits {
    struct sMSGRTNODEBASECFGTypeInfo :
        public RTT::types::StructTypeInfo< sMSGRTNODEBASECFG >
    {
        sMSGRTNODEBASECFGTypeInfo()
            : RTT::types::StructTypeInfo< sMSGRTNODEBASECFG >("/sMSGRTNODEBASECFG") {}



    };

    RTT::types::TypeInfoGenerator* sMSGRTNODEBASECFG_TypeInfo()
    { return new sMSGRTNODEBASECFGTypeInfo(); }

}

/* Generated from orogen/lib/orogen/templates/typekit/TemplateInstanciation.cpp */

#include <rtt/Port.hpp>
#include <rtt/Attribute.hpp>
#include <rtt/Property.hpp>
#include <rtt/internal/DataSource.hpp>

template class RTT::OutputPort< sMSGRTNODEBASECFG >;
template class RTT::InputPort< sMSGRTNODEBASECFG >;
template class RTT::Property< sMSGRTNODEBASECFG >;
template class RTT::Attribute< sMSGRTNODEBASECFG >;

template class RTT::internal::DataSource< sMSGRTNODEBASECFG >;
template class RTT::internal::ValueDataSource< sMSGRTNODEBASECFG >;
template class RTT::internal::ConstantDataSource< sMSGRTNODEBASECFG >;
template class RTT::internal::AssignableDataSource< sMSGRTNODEBASECFG >;
template class RTT::internal::ReferenceDataSource< sMSGRTNODEBASECFG >;



